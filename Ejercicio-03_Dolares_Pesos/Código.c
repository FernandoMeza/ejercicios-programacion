/*
 * Problema: Elabora un programa que reciba como entrada un monto en dólares (USD)
 * y devuelva la cantidad equivalente en pesos mexicanos (MXN) 
 */
#include <stdio.h>
#include <math.h>
#include "LecturaDatos.h"
#define TAM 6
//Número arbitrario
#define TAM_BUFFER 19

int main()
{
    const double TASA_DE_CAMBIO = 20.55;
    double dolares = 0.0;
    double pesos = 0.0;
    const char *mensaje[TAM] = {
        "Programa que convierte dolares en pesos\n", 
        "Ingresa un número positivo decimal (máximo 17 caracteres): ",
        "\nTerminó el programa.\n",
        "Error en la entrada.",
        "El resultado en pesos es: ",
        "El número es negativo"
    };

    printf("%s%s",mensaje[0], mensaje[1]);
    if (conseguir_double(TAM_BUFFER, &dolares) == EXITO)
    {
        if(signbit(dolares) == 0)
        {
            pesos = dolares * TASA_DE_CAMBIO;
            printf("%s%.2f",mensaje[4], pesos);
        }
        else
        {
            printf("%s", mensaje[5]);
        }
    }
    else
        printf("%s", mensaje[3]);

    printf("%s", mensaje[2]);
}