/*
 * Problema: Elabora un programa calcule el perímetro de un triángulo equilátero.
 * El programa pedirá al usuario las entradas necesarias.
 */
#include <stdio.h>
#include <math.h>
#include "LecturaDatos.h"
#define TAM 6
//Número arbitrario
#define TAM_BUFFER 19

int main()
{
    double lado = 0.0;
    double perimetro = 0.0;
    const char *mensaje[TAM] = {
        "Programa que calcula perimetro de triangulo equilatero\n", 
        "Ingresa la medida del lado del triangulo (> 0.0 y máximo 17 carácteres): ",
        "\nTerminó el programa.\n",
        "Error en la entrada.",
        "El perimetro es: ",
        "El número es negativo o 0.0"
    };

    printf("%s%s",mensaje[0], mensaje[1]);
    if (conseguir_double(TAM_BUFFER, &lado) == EXITO)
    {
        if(signbit(lado) == 0 && lado != 0.0)
        {
            perimetro = lado * 3;
            printf("%s%.2f",mensaje[4], perimetro);
        }
        else
        {
            printf("%s", mensaje[5]);
        }
    }
    else
        printf("%s", mensaje[3]);

    printf("%s", mensaje[2]);
}