--Problema: Elabora un programa que reciba un número entre 1 y 50 y devuelva 
--la suma de los números consecutivos del 1 hasta ese número.

Inicio
	Macro TAM 5
	Constante NUMERO_MINIMO es entero
	Constante NUMERO_MAXIMO es entero
	Constante mensajes es arreglo de cadena de caracteres de tamaño TAM
	Variable n es entero
	Variable Resultado es entero

	NUMERO_MINIMO <- 1
	NUMERO_MAXIMO <- 50
	n <- 0
	Resultado <- 0
	mensaje[0] <- "Programa que suma desde 1 hasta n (n es un número entre 1 y 50).\n"
	mensaje[1] <- "Ingresa un número entre 1 y 50 y presiona enter: "
	mensaje[2] <- "\nTerminó el programa.\n"
	mensaje[3] <- "El número no esta en el rango"
	mensaje[4] <- "El resultado es: "
	
	Escribir mensaje[0], mensaje[1]
	Leer n --Esta función solo admite numeros y avanza hasta que sea un número!
	
	Si n >= NUMERO_MINIMO y n <= NUMERO_MAXIMO entonces
		Resultado <- n * (n + 1) / 2
		Escribir mensaje[4], Resultado
	en otro caso
		Escribir mensaje[3]
	
	Escribir mensaje[2]
Fin