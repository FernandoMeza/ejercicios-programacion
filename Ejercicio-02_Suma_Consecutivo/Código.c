/*
 *  Problema: Elabora un programa que reciba un número entre 1 y 50 y devuelva 
 *  la suma de los números consecutivos del 1 hasta ese número. 
 */
#include <stdio.h>
#include "LecturaDatos.h"
#define TAM 6

int main()
{
    const int NUMERO_MINIMO = 1;
    const int NUMERO_MAXIMO = 50;
    const int TAM_BUFFER = 4;
    long int n = 0;
    int resultado = 0;
    const char *mensaje[TAM] = {
        "Programa que suma desde 1 hasta n (n es un número entre 1 y 50).\n", 
        "Ingresa un número entre 1 y 50 y presiona enter: ",
        "\nTerminó el programa.\n",
        "Error en la entrada.",
        "El resultado es: ",
        "El número no esta en el rango"
    };

    printf("%s%s",mensaje[0], mensaje[1]);
    if (conseguir_long_int(TAM_BUFFER, &n) == EXITO)
    {
        if(n >= NUMERO_MINIMO && n <= NUMERO_MAXIMO)
        {
            resultado = (int)n * ((int)n + 1) / 2;
            printf("%s%d",mensaje[4], resultado);
        }
        else
        {
            printf("%s", mensaje[5]);
        }
    }
    else
        printf("%s", mensaje[3]);

    printf("%s", mensaje[2]);
}