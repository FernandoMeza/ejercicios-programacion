#include "LecturaDatos.h"

#define BASE 10

int conseguir_long_int(size_t tam_buffer, long int *numero)
{
    char* buffer = (char *)malloc(sizeof(char)*tam_buffer);
    char* fin;

    if (fgets(buffer, tam_buffer, stdin) != NULL)
    {
        *numero = strtol(buffer, &fin, BASE);
    }

    if (buffer[0] != '\n' && (*fin == '\n'))
    {   
        free(buffer);
        return EXITO;
    }
    else
    {
        free(buffer);
        return ERROR;
    }
}

int conseguir_double(size_t tam_buffer, double *numero)
{
    char* buffer = (char * )malloc(sizeof(char)*tam_buffer);
    char* fin;

    if (fgets(buffer, tam_buffer, stdin) != NULL)
    {
        *numero = strtod(buffer, &fin);
    }

    if (buffer[0] != '\n' && (*fin == '\n'))
    {   
        free(buffer);
        return EXITO;
    }
    else
    {
        free(buffer);
        return ERROR;
    }
}