/*
 * Problema: Elabora un programa calcule el perímetro de un triángulo isósceles.
 * El programa pedirá al usuario las entradas necesarias.
 */
#include <stdio.h>
#include <math.h>
#include "LecturaDatos.h"
#include <stdbool.h>
#define TAM 7
//Número arbitrario
#define TAM_BUFFER 19
#define NUM_LADOS 2
	
bool es_valido(double a, double b, double c)
{
	if (signbit(a)!=0 || signbit(b)!=0 || signbit(c)!=0)
		return false;
	else if (a+b<=c || a+c<=b || b+c<=a)
		return false;
	else
		return true;
}


int main()
{
    double lado[NUM_LADOS] = {0.0,0.0};
    double perimetro = 0.0;
    bool prueba = true;
    const char *mensaje[TAM] = {
        "Programa que calcula perimetro de triangulo isosceles\n", 
		"Ingresa la medida del lado que se repite(> 0.0): ",
		"\nTerminó el programa.\n",
        "Error en la entrada.",
        "El perimetro es: ",
		"El triangulo es invalido",
		"Ingresa la medida del lado que es único(> 0.0): "
    };

    printf("%s%s",mensaje[0], mensaje[1]);
	if (conseguir_double(TAM_BUFFER, &lado[0]) == ERROR)
	{
		printf("%s%s", mensaje[3], mensaje[2]);
		return -1;
	}
	
	printf("%s",mensaje[6]);
	if (conseguir_double(TAM_BUFFER, &lado[1]) == ERROR)
	{
		printf("%s%s", mensaje[3], mensaje[2]);
		return -1;
	}
	
	if (es_valido(lado[0], lado[0], lado[1]))
	{
		perimetro = 2 * lado[0] + lado[1]; 
        printf("%s%.2f",mensaje[4], perimetro);
	}
	else
	{
		printf("%s",mensaje[5]);
	}

    printf("%s", mensaje[2]);
}