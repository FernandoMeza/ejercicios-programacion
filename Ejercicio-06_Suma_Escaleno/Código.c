/*
 * Problema: Elabora un programa que calcule el perímetro de un triángulo escaleno. 
 * El programa pedirá al usuario las entradas necesarias.
 */
#include <stdio.h>
#include <math.h>
#include "LecturaDatos.h"
#include <stdbool.h>
#define TAM 7 
//Número arbitrario
#define TAM_BUFFER 19
#define NUM_LADOS 3

bool es_valido(double a, double b, double c)
{
	if (signbit(a)!=0 || signbit(b)!=0 || signbit(c)!=0)
		return false;
	else if (a+b<=c || a+c<=b || b+c<=a)
		return false;
	else
		return true;
}

int main()
{
    double lado[NUM_LADOS] = {0.0,0.0,0.0};
    double perimetro = 0.0;
    bool prueba = true;
    const char *mensaje[TAM] = {
        "Programa que calcula perimetro de triangulo escaleno\n", 
        "Ingresa la medida del %d lado(> 0.0): ",
        "\nTerminó el programa.\n",
        "Error en la entrada.",
        "El perimetro es: ",
        "El triangulo es invalido",
		"¡Advertencia!, no es triángulo escaleno\n"
    };

    printf("%s",mensaje[0]);

    for (int i = 0; i < NUM_LADOS; ++i)
    {
        printf(mensaje[1], i+1);
        if (conseguir_double(TAM_BUFFER, &lado[i]) == ERROR)
            prueba = false;
    }
	
    if (prueba == true)
    {
        if(es_valido(lado[0], lado[1], lado[2]))
        {
			perimetro = lado[0] + lado[1] + lado[2];
			for (int i = 0; i < NUM_LADOS-1 ; ++i)
				if (lado[i] == lado[i + 1])
					prueba = false;
			if (prueba == false)
				printf("%s", mensaje[6]);
            printf("%s%.2f",mensaje[4], perimetro);
        }
        else
        {
            printf("%s", mensaje[5]);
        }
    }
    else
        printf("%s",mensaje[3]);
    

    printf("%s", mensaje[2]);
}