#ifndef LECTURADATOS_H
#define LECTURADATOS_H

#include <stdlib.h> 
#include <stdio.h>

/*Existen muchas complicaciones para la validación de la entrada en c,
para esto se creó este header.*/

enum retorno{EXITO, ERROR};
int conseguir_long_int(size_t tam_buffer, long int *numero);
int conseguir_double(size_t tam_buffer, double *numero);

#endif //LECTURADATOS_H