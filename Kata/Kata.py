def main():
    romanos = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D':500, 'M': 1000}
    entrada = input("Ingresa dos pares, en mayúsuclas y sin espacios, ej: 4X5M: ")
    if (len(entrada) == 4 and entrada[0].isnumeric() and entrada[2].isnumeric() and
        entrada[1]in romanos.keys() and entrada[3] in romanos.keys()):
        print("Formato correcto.")
    else:
        print("Error en formato.")
        return

    sum = 0
    if (romanos[entrada[1]] >= romanos[entrada[3]]):
        sum = int(entrada[0]) * romanos[entrada[1]] + int(entrada[2]) * romanos[entrada[3]]
    else:
        sum = -(int(entrada[0]) * romanos[entrada[1]]) + int(entrada[2]) * romanos[entrada[3]]

    print("Resultado:", sum)

main()
