/*
 *Problema: Elabora un programa que imprima los números pares del 0 al 100.
 */

#include <stdio.h>
#define TAM 3

int main()
{
    const int LIMITE_INFERIOR = 0;
    const int LIMITE_SUPERIOR = 100;
    const int ELEMENTOS_LINEA = 20;
    const char *mensaje[TAM] = {
        "Programa que imprime los números pares del 0 al 100.\n", 
        "Presiona enter para continuar. ",
        "\n\nTermino el programa.\n"};

    printf("%s%s",mensaje[0], mensaje[1]);
    getchar();

    for (int i = LIMITE_INFERIOR; i <= LIMITE_SUPERIOR; i = i + 2)
    {
        printf("%d ", i);
        if ((i+2) % ELEMENTOS_LINEA == 0)
        {
            printf("\n");
        }
    }

    printf("%s", mensaje[2]);
    
}